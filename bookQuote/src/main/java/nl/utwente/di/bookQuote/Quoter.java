package nl.utwente.di.bookQuote;

        import java.util.HashMap;

        //**Hello
    // updating the code
public class Quoter {

    private HashMap<String, Double> bookPrices;

    public Quoter(){
        bookPrices = new HashMap<String, Double>();
        bookPrices.put("1", 10.0);
        bookPrices.put("2", 45.0);
        bookPrices.put("3", 20.0);
        bookPrices.put("4", 35.0);
        bookPrices.put("5", 50.0);
    }
    public double getBookPrice(String isbn) {
        Double price = bookPrices.getOrDefault(isbn, 0.0);
        return price;
    }
}
